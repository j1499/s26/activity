/*
The questions are as follows:

- What directive is used by Node.js in loading the modules it needs?
	- require

- What Node.js module contains a method for server creation?
	- http

- What is the method of the http object responsible for creating a server using Node.js?
	- createServer()

- What method of the response object allows us to set status codes and content types?
	- writeHead()

- Where will console.log() output its contents when run in Node.js?
	- terminal
	
- What property of the request object contains the address's endpoint?
	- url
*/

let http = require("http");

const port = 3000;

const server = http.createServer( (request, response) => {
		if (request.url == '/login'){
			response.writeHead(200, {'Content-type': 'text/plain'});
			response.end("You are in the login page");
		} else {
			response.writeHead(404, {'Content-Type': 'text/plain'});
			response.end("Page not available");
		}

});

server.listen(port);

console.log(`Server now accessible at localhost: ${port}.`);

